/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.amdatu.kubernetes.Kubernetes;
import org.amdatu.kubernetes.Predicates;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.osgi.framework.FrameworkUtil;

import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.NamespaceList;
import io.fabric8.kubernetes.api.model.NodeList;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.ReplicationControllerBuilder;
import io.fabric8.kubernetes.api.model.ReplicationControllerList;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.SecretList;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceBuilder;
import io.fabric8.kubernetes.api.model.ServiceList;
import rx.Observable;
import rx.observers.TestSubscriber;

//@Ignore
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KubernetesTest {
    private static final String REPLICATION_CONTROLLER_NAME = "testrc";
    private static final String TEST_APP_LABEL = "test-app";
    private static final String NAMESPACE = "testns";

    private volatile Kubernetes kubernetes;

    @Before
    public void setup() {
        String k8sUrl = FrameworkUtil.getBundle(getClass()).getBundleContext().getProperty("kubernetesURL");
        if (k8sUrl == null) {
            k8sUrl = System.getenv("KUBERNETES_URL");
        }

        Assume.assumeNotNull(k8sUrl);
        configure(this)
            .add(createServiceDependency().setService(Kubernetes.class).setRequired(true))
            .add(createConfiguration("org.amdatu.kubernetes")
                .set("kubernetesurl", k8sUrl))
            .apply();
    }

    @After
    public void after() {
        cleanUp(this);
    }

    @Test
    public void step01_createNamespace() throws InterruptedException {
        TestSubscriber<Namespace> testSubscriber = new TestSubscriber<>();

        kubernetes.deleteNamespace(NAMESPACE).subscribe(r -> {} , error -> {
            fail("NS not deleted");
        } , () -> {
            Namespace ns = new NamespaceBuilder().withNewMetadata().withName(NAMESPACE).endMetadata().build();

            kubernetes.createNamespace(ns).subscribe(testSubscriber);
        });

        awaitTerminalEvent(testSubscriber);

        List<Namespace> namespaces = testSubscriber.getOnNextEvents();
        assertEquals(NAMESPACE, namespaces.get(0).getMetadata().getName());
    }

    @Test
    public void step01b_listNamespaces() throws InterruptedException {
        TestSubscriber<NamespaceList> testSubscriber = new TestSubscriber<>();

        kubernetes.listNamespaces().subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<NamespaceList> namespaces = testSubscriber.getOnNextEvents();
        assertTrue(namespaces.get(0).getItems().size() > 0);
    }

    @Test
    public void step02_createRC() {
        createRc();
    }

	private void createRc() {
		Map<String, String> selector = new HashMap<>();
        selector.put("name", TEST_APP_LABEL);

        ReplicationController rc = new ReplicationControllerBuilder()
            .withNewMetadata().withName(REPLICATION_CONTROLLER_NAME).endMetadata()
            .withNewSpec()
            .withSelector(selector)
            .withReplicas(0)
            .withNewTemplate().withNewSpec()
            .addNewContainer()
            .withName("nginx")
            .withImage("nginx")
            .endContainer()
            .endSpec()
            .withNewMetadata().withLabels(selector).endMetadata()
            .endTemplate()
            .endSpec()
            .build();

        TestSubscriber<ReplicationController> testSubscriber = new TestSubscriber<>();
        kubernetes.createReplicationController(NAMESPACE, rc).subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<ReplicationController> rcs = testSubscriber.getOnNextEvents();
        assertEquals(0, rcs.get(0).getStatus().getReplicas().intValue());
	}

    @Test
    public void step03_scaleRc() {

        TestSubscriber<ReplicationController> testSubscriber = new TestSubscriber<>();
        kubernetes.getReplicationController(NAMESPACE, REPLICATION_CONTROLLER_NAME).subscribe(rc -> {
            rc.getSpec().setReplicas(2);
            kubernetes.updateReplicationController(NAMESPACE, rc).subscribe(testSubscriber);
        });

        awaitTerminalEvent(testSubscriber);

        List<ReplicationController> rcs = testSubscriber.getOnNextEvents();

        assertEquals(2, rcs.get(0).getSpec().getReplicas().intValue());
    }

    @Test
    public void step04_waitForPods() throws InterruptedException {

        Map<String, String> labels = new HashMap<>();
        labels.put("name", TEST_APP_LABEL);

        TestSubscriber<PodList> testSubscriber = new TestSubscriber<>();

        kubernetes.waitForPods(NAMESPACE, labels, 2, Predicates.RUNNING)
            .timeout(10, TimeUnit.SECONDS)
            .subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<PodList> podLists = testSubscriber.getOnNextEvents();
        assertEquals(2, podLists.get(0).getItems().size());
    }

    @Test
    public void step05_getPods() throws InterruptedException {

        Map<String, String> labels = new HashMap<>();
        labels.put("name", TEST_APP_LABEL);

        TestSubscriber<PodList> testSubscriber = new TestSubscriber<>();

        kubernetes.listPods(NAMESPACE, labels)
            .subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<PodList> podLists = testSubscriber.getOnNextEvents();
        assertEquals(2, podLists.get(0).getItems().size());
    }

    @Test
    public void step05b_getPodsWithNotExistingLabel() throws InterruptedException {

        Map<String, String> labels = new HashMap<>();
        labels.put("name", "not-existing");

        TestSubscriber<PodList> testSubscriber = new TestSubscriber<>();

        kubernetes.listPods(NAMESPACE, labels)
            .subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<PodList> podLists = testSubscriber.getOnNextEvents();
        assertEquals(0, podLists.get(0).getItems().size());
    }

    @Test
    public void step06_getSinglePod() throws InterruptedException {

        Map<String, String> labels = new HashMap<>();
        labels.put("name", TEST_APP_LABEL);
        TestSubscriber<Pod> testSubscriber = new TestSubscriber<>();

        kubernetes.listPods(NAMESPACE, labels)
            .flatMap(l -> Observable.from(l.getItems()))
            .first()
            .flatMap(p -> kubernetes.getPod(NAMESPACE, p.getMetadata().getName()))
            .subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<Pod> pods = testSubscriber.getOnNextEvents();
        assertEquals(TEST_APP_LABEL, pods.get(0).getMetadata().getLabels().get("name"));
    }

    @Test
    public void step07_getNodes() {
        TestSubscriber<NodeList> testSubscriber = new TestSubscriber<>();

        kubernetes.listNodes().subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<NodeList> nodesList = testSubscriber.getOnNextEvents();
        assertTrue(nodesList.get(0).getItems().size() > 0);

    }

    @Test
    public void step08_deletePod() {
    	Map<String, String> labels = new HashMap<>();
        labels.put("name", TEST_APP_LABEL);
        TestSubscriber<String> testSubscriber = new TestSubscriber<>();

        Observable<String> listPods = kubernetes.listPods(NAMESPACE, labels)
            .flatMap(l -> Observable.from(l.getItems()))
            .map(p -> p.getMetadata().getName());
		
        listPods.subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);
        List<String> pods = testSubscriber.getOnNextEvents();
        
        TestSubscriber<Boolean> deleteSubscriber = new TestSubscriber<>();        
        kubernetes.deletePod(NAMESPACE, pods.get(0)).subscribe(deleteSubscriber);
        awaitTerminalEvent(deleteSubscriber);
        deleteSubscriber.assertReceivedOnNext(Arrays.asList(true));
        
        TestSubscriber<String> listSubscriber = new TestSubscriber<>();

       listPods.subscribe(listSubscriber);
        awaitTerminalEvent(listSubscriber);
        List<String> pods2 = listSubscriber.getOnNextEvents();
        
        assertFalse(pods.equals(pods2));
    }
    
    @Test
    public void step09_watchPods() {
        TestSubscriber<String> testSubscriber = new TestSubscriber<>();

        kubernetes.watchPods(NAMESPACE, null)
            .filter(p -> "DELETED".equals(p.getType()))
            .takeUntil(p -> "DELETED".equals(p.getType()))
            .map(u -> u.getType())
            .subscribe(testSubscriber);

        kubernetes.scale(NAMESPACE, REPLICATION_CONTROLLER_NAME, 1).subscribe();

        awaitTerminalEvent(testSubscriber);

        testSubscriber.assertReceivedOnNext(Arrays.asList("DELETED"));
    }

    @Test
    public void step10_secrets() {
        TestSubscriber<Secret> secretSubscriber = new TestSubscriber<>();
        
        // create
        String secretName = "testsecret";
        Map<String, String> data = new HashMap<>();
        data.put("key1", encode("value1a"));
        data.put("key2", encode("value2a"));
        
        kubernetes.setSecret(NAMESPACE, secretName, data).subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);
        Secret secret = secretSubscriber.getOnNextEvents().get(0);
        assertEquals(secretName, secret.getMetadata().getName());
        assertEquals("value1a", decode(secret.getData().get("key1")));
        assertEquals("value2a", decode(secret.getData().get("key2")));
        
        // get
        secretSubscriber = new TestSubscriber<>();
        kubernetes.getSecret(NAMESPACE, secretName).subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);
        secret = secretSubscriber.getOnNextEvents().get(0);
        assertEquals(secretName, secret.getMetadata().getName());
        assertEquals("value1a", decode(secret.getData().get("key1")));
        assertEquals("value2a", decode(secret.getData().get("key2")));
        
        // update
        secretSubscriber = new TestSubscriber<>();
        data.put("key1", encode("value1b"));
        data.put("key2", encode("value2b"));
        kubernetes.setSecret(NAMESPACE, secretName, data).subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);
        secret = secretSubscriber.getOnNextEvents().get(0);
        assertEquals(secretName, secret.getMetadata().getName());
        assertEquals("value1b", decode(secret.getData().get("key1")));
        assertEquals("value2b", decode(secret.getData().get("key2")));

        // get updated
        secretSubscriber = new TestSubscriber<>();
        kubernetes.getSecret(NAMESPACE, secretName).subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);
        secret = secretSubscriber.getOnNextEvents().get(0);
        assertEquals(secretName, secret.getMetadata().getName());
        assertEquals("value1b", decode(secret.getData().get("key1")));
        assertEquals("value2b", decode(secret.getData().get("key2")));

        // create 2nd
        secretSubscriber = new TestSubscriber<>();
        String secretName2 = "testsecret2";
        data = new HashMap<>();
        data.put("key11", encode("value11"));
        data.put("key22", encode("value22"));
        kubernetes.setSecret(NAMESPACE, secretName2, data).subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);

        // list
        TestSubscriber<SecretList> secretListSubscriber = new TestSubscriber<>();
        kubernetes.listSecrets(NAMESPACE).subscribe(secretListSubscriber);
        awaitTerminalEvent(secretListSubscriber);
        SecretList list = secretListSubscriber.getOnNextEvents().get(0);
        assertEquals(2, list.getItems().size());
        
        // delete
        TestSubscriber<Boolean> booleanSubscriber = new TestSubscriber<>();
        kubernetes.deleteSecret(NAMESPACE, secretName).subscribe(booleanSubscriber);
        awaitTerminalEvent(secretSubscriber);
        booleanSubscriber.assertReceivedOnNext(Arrays.asList(Boolean.TRUE));
        
        // delete 2nd
        kubernetes.deleteSecret(NAMESPACE, secretName2);

        // get deleted
        secretSubscriber = new TestSubscriber<>();
        kubernetes.getSecret(NAMESPACE, secretName).subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);
        secretSubscriber.assertReceivedOnNext(Arrays.asList((Secret)null));

        // create with Secret object
        secretSubscriber = new TestSubscriber<>();
        Secret newSecretObject = new Secret();
        ObjectMeta meta = new ObjectMeta();
        meta.setName(secretName);
        newSecretObject.setMetadata(meta);
        HashMap<String, String> newData = new HashMap<>();
        newData.put("another-key", encode("anotherValue"));
        newSecretObject.setData(newData);
        kubernetes.setSecret(NAMESPACE, newSecretObject)
        	// update with Secret object
        	.flatMap(s -> {
            	s.getData().put("aaa", encode("bbb"));
            	return kubernetes.setSecret(NAMESPACE, s);
        	})
        	.subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);

        // get updated
        secretSubscriber = new TestSubscriber<>();
        kubernetes.getSecret(NAMESPACE, secretName).subscribe(secretSubscriber);
        awaitTerminalEvent(secretSubscriber);
        secret = secretSubscriber.getOnNextEvents().get(0);
        assertEquals(secretName, secret.getMetadata().getName());
        assertEquals("anotherValue", decode(secret.getData().get("another-key")));
        assertEquals("bbb", decode(secret.getData().get("aaa")));
        
        kubernetes.deleteSecret(NAMESPACE, secretName);

    }
    
    @Test
    public void step11_listReplicationControllers() {
    	TestSubscriber<ReplicationControllerList> testSubscriber = new TestSubscriber<>();
    	kubernetes.listReplicationControllers(NAMESPACE).subscribe(testSubscriber);
    	
    	testSubscriber.awaitTerminalEvent();
    	testSubscriber.assertNoErrors();
    	
    	testSubscriber.assertValueCount(1);
    }
    
    @Test
    public void step12_services() {
    	
    	String label = TEST_APP_LABEL;
    	String serviceName = "testsrv";
    	
    	// create
    	Map<String, String> selector = new HashMap<>();
        selector.put("name", label);
        
        Service service = new ServiceBuilder()
        		.withNewMetadata().withName(serviceName).endMetadata()
        		.withNewSpec()
        		.withSelector(selector)
        		.addNewPort()
        		.withPort(80)
        		.endPort()
        		.endSpec()
        		.build();

        TestSubscriber<Service> testSubscriber = new TestSubscriber<>();
        kubernetes.createService(NAMESPACE, service).subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        List<Service> services = testSubscriber.getOnNextEvents();
        assertTrue(services.get(0).getMetadata().getName().equals(serviceName));
        
        // create second service
        // selector -> name=test-app2, label -> name=test-app2
        label = "test-app2";
        serviceName = "testsrv2";
        selector = new HashMap<>();
        selector.put("name", label);
        
        service = new ServiceBuilder()
        		.withNewMetadata().withName(serviceName)
        		.withLabels(selector)
        		.endMetadata()
        		.withNewSpec()
        		.withSelector(selector)
        		.addNewPort()
        		.withPort(80)
        		.endPort()
        		.endSpec()
        		.build();

        testSubscriber = new TestSubscriber<>();
        kubernetes.createService(NAMESPACE, service).subscribe(testSubscriber);

        awaitTerminalEvent(testSubscriber);

        services = testSubscriber.getOnNextEvents();
        assertTrue(services.get(0).getMetadata().getName().equals(serviceName));        

        // get
        testSubscriber = new TestSubscriber<>();
        kubernetes.getService(NAMESPACE, serviceName).subscribe(testSubscriber);
        awaitTerminalEvent(testSubscriber);
        
        services = testSubscriber.getOnNextEvents();
        assertEquals(1, services.size());
        
        // list services
        TestSubscriber<ServiceList> testSubscriber2 = new TestSubscriber<ServiceList>();
        
        kubernetes.listServices(NAMESPACE).subscribe(testSubscriber2);
        
        awaitTerminalEvent(testSubscriber2);
        List<ServiceList> services2 = testSubscriber2.getOnNextEvents();
        assertEquals(2, services2.get(0).getItems().size());
        
        // list services with labels
        Map<String, String> labelSelector = new HashMap<>();
        labelSelector.put("name", "test-app2");
        
        testSubscriber2 = new TestSubscriber<ServiceList>();
        
        kubernetes.listServices(NAMESPACE, labelSelector).subscribe(testSubscriber2);
        
        awaitTerminalEvent(testSubscriber2);
        services2 = testSubscriber2.getOnNextEvents();
        assertEquals(1, services2.get(0).getItems().size());
        
        // update, change port to 8080

        /// get
        serviceName = "testsrv";

        testSubscriber = new TestSubscriber<>();
        kubernetes.getService(NAMESPACE, serviceName).subscribe(testSubscriber);
        awaitTerminalEvent(testSubscriber);
        
        services = testSubscriber.getOnNextEvents();
        service = services.get(0);
        service.getSpec().getPorts().get(0).setPort(8080);
        
        /// update
        testSubscriber = new TestSubscriber<>();
        kubernetes.updateService(NAMESPACE, service).subscribe(testSubscriber);
        
        awaitTerminalEvent(testSubscriber);
        services = testSubscriber.getOnNextEvents();
        
        Service updatedService = services.get(0);
        
        assertEquals(new Integer(8080), updatedService.getSpec().getPorts().get(0).getPort());
        
        // delete
        serviceName = "testsrv";
        TestSubscriber<Boolean> testSubscriber3 = new TestSubscriber<Boolean>();
        kubernetes.deleteService(NAMESPACE, serviceName).subscribe(testSubscriber3);
        awaitTerminalEvent(testSubscriber3);
        
        List<Boolean> results = testSubscriber3.getOnNextEvents();
        assertTrue(results.get(0));
      
    }
    
    
    private String encode(String s) {
    	return Base64.getEncoder().encodeToString(s.getBytes());
    }
    
    private String decode(String s) {
    	return new String(Base64.getDecoder().decode(s));
    }
    
    private <T> void awaitTerminalEvent(TestSubscriber<T> subscriber) {
        subscriber.awaitTerminalEvent(10, TimeUnit.SECONDS);
        subscriber.assertNoErrors();
        subscriber.assertCompleted();
    }
}
