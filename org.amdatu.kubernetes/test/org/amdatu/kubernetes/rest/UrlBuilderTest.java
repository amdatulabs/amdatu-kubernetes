/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.rest;

import static org.junit.Assert.assertEquals;

import java.net.URI;

import org.junit.Test;

public class UrlBuilderTest {
    private URI m_baseURL = URI.create("http://localhost:8080");

    @Test
    public void testSinglePath() {
        String url = new UrlBuilder(m_baseURL, "v1").namespace("default").path("pods").build();
        assertEquals("http://localhost:8080/api/v1/namespaces/default/pods", url);
    }

    @Test
    public void testPathsWithExtraSlashes() {
        String url = new UrlBuilder(URI.create("http://localhost:8080/"), "v1").namespace("/default/").path("/pods/").build();
        assertEquals("http://localhost:8080/api/v1/namespaces/default/pods", url);
    }

    @Test
    public void testNoPath() {
        String url = new UrlBuilder(m_baseURL, "v1").namespace("default").build();
        assertEquals("http://localhost:8080/api/v1/namespaces/default", url);
    }

    @Test
    public void testMultiPath() {
        String url;

        url = new UrlBuilder(m_baseURL, "v1").namespace("default").path("pods").path("mypod").build();
        assertEquals("http://localhost:8080/api/v1/namespaces/default/pods/mypod", url);

        url = new UrlBuilder(m_baseURL, "v1").path("pods").path("mypod").build();
        assertEquals("http://localhost:8080/api/v1/pods/mypod", url);
    }

    @Test
    public void testPathWithLabels() {
        String url;

        url = new UrlBuilder(m_baseURL, "v1").namespace("default").path("pods").label("foo", "bar").label("qux", "quu").build();
        assertEquals("http://localhost:8080/api/v1/namespaces/default/pods?labelSelector=foo%3Dbar,qux%3Dquu", url);

        url = new UrlBuilder(m_baseURL, "v1").namespace("default").path("pods").label("foo", "bar").buildWs();
        assertEquals("ws://localhost:8080/api/v1/namespaces/default/pods?labelSelector=foo%3Dbar&watch=true", url);        
    }
    
    @Test
    public void testPathWithQuery() {
        String url;

        url = new UrlBuilder(m_baseURL, "v1").namespace("default").path("pods").query("foo", "bar").query("hello", "world").build();
        assertEquals("http://localhost:8080/api/v1/namespaces/default/pods?foo=bar&hello=world", url);
    }
    
    @Test
    public void testPathWithQueryAndLabels() {
        String url;

        url = new UrlBuilder(m_baseURL, "v1").namespace("default").path("pods").label("foo", "bar").query("hello", "world").label("qux", "quu").build();
        assertEquals("http://localhost:8080/api/v1/namespaces/default/pods?labelSelector=foo%3Dbar,qux%3Dquu&hello=world", url);
    }
}
