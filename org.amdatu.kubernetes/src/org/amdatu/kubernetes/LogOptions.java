package org.amdatu.kubernetes;

public class LogOptions {

	private boolean m_follow;
	private long m_limitBytes;
	private boolean m_previous;
	private long m_since;
	private String m_sinceTime;
	private int m_tail;
	private boolean m_timestamps;

	public boolean isFollow() {
		return m_follow;
	}

	public void setFollow(boolean follow) {
		m_follow = follow;
	}

	public long getLimitBytes() {
		return m_limitBytes;
	}

	public void setLimitBytes(long limitBytes) {
		m_limitBytes = limitBytes;
	}

	public boolean isPrevious() {
		return m_previous;
	}

	public void setPrevious(boolean previous) {
		m_previous = previous;
	}

	public long getSince() {
		return m_since;
	}

	public void setSince(long since) {
		m_since = since;
	}

	public String getSinceTime() {
		return m_sinceTime;
	}

	public void setSinceTime(String sinceTime) {
		m_sinceTime = sinceTime;
	}

	public int getTail() {
		return m_tail;
	}

	public void setTail(int tail) {
		m_tail = tail;
	}

	public boolean isTimestamps() {
		return m_timestamps;
	}

	public void setTimestamps(boolean timestamps) {
		m_timestamps = timestamps;
	}

	static class Builder {
		private LogOptions m_options;
		
		public Builder() {
			m_options = new LogOptions();
		}
		
		public static Builder create() {
			return new Builder();
		}
		
		public LogOptions build() {
			return m_options;
		}
		
		public Builder follow() {
			m_options.m_follow = true;
			return this;
		}
		
		public Builder limitBytes(long bytes) {
			m_options.m_limitBytes = bytes;
			return this;
		}
		
		public Builder previous() {
			m_options.m_previous = true;
			return this;
		}
		
		public Builder since(long since) {
			m_options.m_since = since;
			return this;
		}
		
		public Builder sinceTime(String time) {
			m_options.m_sinceTime = time;
			return this;
		}
		
		public Builder tail(int tail) {
			m_options.m_tail = tail;
			return this;
		}
		
		public Builder timestamps() {
			m_options.m_timestamps = true;
			return this;
		}
	}
}
