/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes;

import java.util.Map;
import java.util.function.Predicate;

import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceList;
import io.fabric8.kubernetes.api.model.NodeList;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.ReplicationControllerList;
import io.fabric8.kubernetes.api.model.ResourceQuotaList;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.SecretList;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceList;
import rx.Observable;

public interface Kubernetes {
	/** Pods **/
	
	/**
	 * List Pods that match the labelSelector.
	 * @param namespace The Kubernetes namespace.
	 * @param labelSelector A map of key value pairs for the labels to filter for. Null is allowed to retrieve all Pods.
	 * @return A PodList, which contains "items" that contain the Pod data.
	 */
	Observable<PodList> listPods(String namespace, Map<String,String> labelSelector);
	
	/**
	 * Find a single Pod by name
	 * @param namespace The Kubernetes namespace.
	 * @param name The name of the Pod
	 * @return The Pod representation.
	 */
	Observable<Pod> getPod(String namespace, String name);
	
	/**
	 * Delete a single Pod by name
	 * @param namespace The Kubernetes namespace.
	 * @param name The name of the Pod
	 * @return The Pod representation.
	 */
	Observable<Boolean> deletePod(String namespace, String name);
	
	
	/**
	 * Wait for a predefined number of Pods that match the predicate become available within the label selector.
	 * This method is useful when a next operation should start after Pods are spinned up.
	 * @param namespace The Kubernetes namespace.
	 * @param labelSelector A map of key value pairs for the labels to filter for. Null is allowed to retrieve all Pods.
	 * @param expected The number of Pods to wait for
	 * @param filter A predicate on the properties of the Pods. If the predicate does not match a new Pod, the Pod doesn't count towards the expected number.
	 * @return The list of matching Pods
	 */
	Observable<PodList> waitForPods(String namespace, Map<String,String> labelSelector, int expected, Predicate<Pod> filter);
	
	/**
	 * Watch for Pod changes. Opens a WebSocket to listen to all changes to Pods within the label selector. 
	 * Combine with RX's takeUntil method to unsubscribe the watch, and combine with RX's filter method to filter on types of events.
	 * @param namespace The Kubernetes namespace.
	 * @param labelSelector A map of key value pairs for the labels to filter for. Null is allowed to retrieve all Pods.
	 * @return An observable that will emit on every pod change.
	 */
	Observable<WatchUpdate<Pod>> watchPods(String namespace, Map<String,String> labelSelector);
	
	/** Namespaces **/
	/**
	 * Create a namespace. Emits and completes when the namespace is created.
	 * @param ns The Kubernetes namespace.
	 * @return The created namespace.
	 */
	Observable<Namespace> createNamespace(Namespace ns);
	
	Observable<NamespaceList> listNamespaces();
	
	/**
	 * Delete a namespace by name. Polls the API until the namespace is fully deleted, which may take a while, and emits/comletes after that.
	 * @param name The namespace to delete
	 * @return True if the namespace was deleted. False if the namespace doesn't exist.
	 */
	Observable<Boolean> deleteNamespace(String name);
	
	/** ReplicationControllers **/
	/**
	 * Get a ReplicationController by name.
	 * @param namespace The Kubernetes namespace.
	 * @param name The name of the RC
	 * @return The matching replication controller
	 */
	Observable<ReplicationController> getReplicationController(String namespace, String name);
	
	/** ReplicationControllers **/
	/**
	 * List replication controllers
	 * @param namespace The Kubernetes namespace.
	 * @return ReplicationConrollers in the given namespace
	 */
	Observable<ReplicationControllerList> listReplicationControllers(String namespace);
	
	/** ReplicationControllers **/
	/**
	 * List replication controllers
	 * @param namespace The Kubernetes namespace.
	 * @param labelSelector A label selector
	 * @return ReplicationConrollers in the given namespace
	 */
	Observable<ReplicationControllerList> listReplicationControllers(String namespace, Map<String,String> labelSelector);
	
	
	/**
	 * Create a new ReplicationController. This will in turn create Pods in the cluster.
	 * @param namespace The Kubernetes namespace.
	 * @param rc The ReplicationController object. Use the ReplicationControllerBuilder to create it.
	 * @return The created ReplicationController
	 */
	Observable<ReplicationController> createReplicationController(String namespace, ReplicationController rc);
	
	/**
	 * Update a ReplicationController by overwriting the current version. This uses a HTTP PUT on the underlying REST API.
	 * @param namespace The Kubernetes namespace.
	 * @param rc The new representation of the ReplicationController
	 * @return The updated ReplicationController
	 */
	Observable<ReplicationController> updateReplicationController(String namespace, ReplicationController rc);
	
	/**
	 * Scale a ReplicationController. The Observable completes immediately after updating the ReplicationController. 
	 * This does NOT mean the Pods are up and running. Use a watch to check this.
	 * @param namespace The Kubernetes namespace.
	 * @param rcName The name of the ReplicationController to scale.
	 * @param nrOfReplicas The new number of replicas.
	 * @return The updated ReplicationController
	 */
	Observable<ReplicationController> scale(String namespace, String rcName, int nrOfReplicas);
	
	/** Nodes **/
	/**
	 * List the available nodes in the Kubernetes cluster.
	 * @return The list of Nodes
	 */
	Observable<NodeList> listNodes();
	
	/** Secrets **/
	
	/**
	 * List Secrets in the namespace. Values are Base64 encoded!
	 * @param namespace The Kubernetes namespace.
	 * @return A SecretList, which contains "items" that contain the Secret data. Values are Base64 encoded!
	 */
	Observable<SecretList> listSecrets(String namespace);

	/**
	 * Find a Secret by name. Values are Base64 encoded!
	 * @param namespace The Kubernetes namespace.
	 * @param name The Secret's name.
	 * @return The Secret. Values are Base64 encoded!
	 */
	Observable<Secret> getSecret(String namespace, String name);

	/**
	 * Add or update Secret in the namespace.
	 * @param namespace The Kubernetes namespace.
	 * @param name The Secret's name.
	 * @param data The Secret's data as a key-value map. Values must be Base64 encoded!
	 * @return The added or updated Secret.
	 */
	Observable<Secret> setSecret(String namespace, String name, Map<String, String> data);

	/**
	 * Add or update Secret in the namespace.
	 * @param namespace The Kubernetes namespace.
	 * @param secret The Secret.
	 * @return The added or updated Secret.
	 */
	Observable<Secret> setSecret(String namespace, Secret secret);

	/**
	 * Remove Secret of the namespace.
	 * @param namespace The Kubernetes namespace.
	 * @param name The Secret's name.
	 * @return True if the Secret was deleted. False if the Secret doesn't exist.
	 */
	Observable<Boolean> deleteSecret(String namespace, String name);

	/** Logs **/	
	
	/**
	 * Get logs for given namespace and pod
	 * @param namespace The namespace
	 * @param podName The pod name
	 * @return The logs of that pod
	 */
	Observable<String> getLogs(String namespace, String podName);
	
	
	/**
	 * Get logs for given namespace, pod and container
	 * @param namespace The namespace
	 * @param podName The pod name
	 * @param containerName The container name
	 * @return The logs of that container
	 */
	Observable<String> getLogs(String namespace, String podName, String containerName);
	
	
	/**
	 * Get logs for given namespace, pod and container, with given options
	 * @param namespace The namespace
	 * @param podName The pod name
	 * @param containerName The container name
	 * @param options The log options
	 * @return The logs of that container
	 */
	Observable<String> getLogs(String namespace, String podName, String containerName, LogOptions options);
	
	/** Services **/
	
	/**
	 * Create a new Service.
	 * @param namespace The Kubernetes namespace.
	 * @param service The Service object. 
	 * @return The created Service.
	 */
	Observable<Service> createService(String namespace, Service service);
	
	/**
	 * Retrieve a Service by name
	 * @param namespace The Kubernetes namespace
	 * @param name The Service name
	 * @return The matching Service
	 */
	Observable<Service> getService(String namespace, String name);
	
	/**
	 * Update a service by overwriting the current version.
	 * @param namespace The Kubernetes namespace.
	 * @param service The new representation of the Service.
	 * @return The updated Service
	 */
	Observable<Service> updateService(String namespace, Service service);
	
	/**
	 * Delete a single Service by name.
	 * @param namespace The Kubernetes namespace.
	 * @param name The Service name
	 * @return True if the Service was deleted, False if the Service doesn't exist.
	 */
	Observable<Boolean> deleteService(String namespace, String name);
	
	/**
	 * List Services from a namespace.
	 * @param namespace The Kubernetes namespace.
	 * @return Services in the given namespace
	 */
	Observable<ServiceList> listServices(String namespace);
	
	/**
	 * List Services from a namespace and that match the label selector
	 * @param namespace The Kubernetes namespace
	 * @param labelSelector A map of key-value pairs fot he labels to filter for. Null is allowed to retrieve all services in the namespace
	 * @return Services in the given namespace and that match the given label selector
	 */
	Observable<ServiceList> listServices(String namespace, Map<String, String> labelSelector);
	
	Observable<ResourceQuotaList> listQuota(String namespace);
	
	/**
	 * @return The namespace we are in, if running in cluster 
	 */
	String getInClusterNamespace();

	/**
	 *
	 * @return the URL of the Kubernetes master
	 */
	String getKubernetesMasterUrl();

	/**
	 *
	 * @return true if the reponse returned by a call to the /healthz endpoint returns 200ok
	 */
	Observable<Boolean> isHealthy();
}
