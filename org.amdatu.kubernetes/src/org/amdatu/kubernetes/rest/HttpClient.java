/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.rest;

import java.io.IOException;

import org.amdatu.kubernetes.WatchUpdate;
import org.glassfish.grizzly.http.util.Header;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.AsyncHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClient.BoundRequestBuilder;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.HttpResponseBodyPart;
import com.ning.http.client.HttpResponseHeaders;
import com.ning.http.client.HttpResponseStatus;
import com.ning.http.client.Response;
import com.ning.http.client.providers.grizzly.GrizzlyAsyncHttpProvider;
import com.ning.http.client.ws.WebSocket;
import com.ning.http.client.ws.WebSocketTextListener;
import com.ning.http.client.ws.WebSocketUpgradeHandler;

import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class HttpClient {
    private final AsyncHttpClient m_client;
    private final ObjectMapper m_mapper;
    private final Config m_config;
    
    public HttpClient(Config config) {
        AsyncHttpClientConfig sc = config.createHttpClientConfig();

        m_client = new AsyncHttpClient(new GrizzlyAsyncHttpProvider(sc));
        
        m_mapper = new ObjectMapper();
        m_config = config;
    }

    public HttpClient(AsyncHttpClient client) {
        m_client = client;
        m_mapper = new ObjectMapper();
        m_config = null;
    }

    public void close() {
        m_client.close();
    }

    public void deleteObject(String url, Subscriber<? super Integer> observer) {
        BoundRequestBuilder requestBuilder = m_client.prepareDelete(url);
        if (m_config.getClientToken() != null) {
            requestBuilder.addHeader(Header.Authorization.toString(), m_config.getClientToken());
        }

        Observable.from(requestBuilder.execute())
            .subscribe(new Subscriber<Response>() {
                @Override
                public void onCompleted() {
                    observer.onCompleted();
                }

                @Override
                public void onError(Throwable t) {
                    observer.onError(t);
                }

                @Override
                public void onNext(Response resp) {
                    int sc = resp.getStatusCode();
                    observer.onNext(sc);
                    observer.onCompleted();
                }
            });
    }

    public void getStatus(String url, Subscriber<? super Integer> observer) {
        BoundRequestBuilder reqBuilder = m_client.prepareGet(url);
        if (m_config.getClientToken() != null) {
            reqBuilder.addHeader(Header.Authorization.toString(), m_config.getClientToken());
        }

        Observable.from(reqBuilder.execute())
            .subscribe(new Subscriber<Response>() {
                @Override
                public void onCompleted() {
                    observer.onCompleted();
                }

                @Override
                public void onError(Throwable t) {
                    observer.onError(t);
                }

                @Override
                public void onNext(Response resp) {
                    int sc = resp.getStatusCode();
                    observer.onNext(sc);
                    observer.onCompleted();
                }
            });
    }

    public <T> void getObject(String url, Class<T> type, Subscriber<? super T> observer) {
        invoke(m_client.prepareGet(url), type, observer);
    }
    
    public void getString(String url, Subscriber<? super String> observer) {
    	invoke(m_client.prepareGet(url), String.class, observer);
    }

    public <T> void postObject(String url, T content, Subscriber<? super T> observer) {
        try {
            @SuppressWarnings("unchecked")
            Class<T> type = (Class<T>) content.getClass();
            invoke(m_client.preparePost(url).setBody(m_mapper.writeValueAsBytes(content)), type, observer);
        }
        catch (JsonProcessingException e) {
            observer.onError(new RuntimeException("Failed to post object: " + content, e));
        }
    }

    public <T> void putObject(String url, T content, Subscriber<? super T> observer) {
        try {
            @SuppressWarnings("unchecked")
            Class<T> type = (Class<T>) content.getClass();
            invoke(m_client.preparePut(url).setBody(m_mapper.writeValueAsBytes(content)), type, observer);
        }
        catch (JsonProcessingException e) {
            observer.onError(new RuntimeException("Failed to put object: " + content, e));
        }
    }
    
    public <T> void patchObject(String url, T content, Subscriber<? super T> observer) {
    	try {
            @SuppressWarnings("unchecked")
            Class<T> type = (Class<T>) content.getClass();
            invoke(m_client.preparePatch(url).setHeader("Content-Type", "application/merge-patch+json").setBody(m_mapper.writeValueAsBytes(content)), type, observer);
        }
        catch (JsonProcessingException e) {
            observer.onError(new RuntimeException("Failed to patch object: " + content, e));
        }
    }

    public <T> Observable<WebSocket> watchObject(String url, Class<T> watchType, Subscriber<? super WatchUpdate<T>> observer) {
        WebSocketUpgradeHandler.Builder builder = new WebSocketUpgradeHandler.Builder();
        builder.addWebSocketListener(new WebSocketTextListener() {
            @Override
            public void onClose(WebSocket websocket) {
                observer.onCompleted();
            }

            @Override
            public void onError(Throwable t) {
                observer.onError(t);
            }

            @Override
            public void onMessage(String message) {
                try {
                    JsonNode jsonNode = m_mapper.readValue(message, JsonNode.class);
                    String type = jsonNode.get("type").asText();
                    JsonNode obj = jsonNode.get("object");

                    T readValue = m_mapper.readValue(obj.toString(), watchType);
                    observer.onNext(new WatchUpdate<T>(type, readValue));
                }
                catch (IOException e) {
                    observer.onError(new RuntimeException("Failed to read object: " + message + " from: " + url, e));
                }
            }

            @Override
            public void onOpen(WebSocket websocket) {
                observer.onStart();
            }
        });

        BoundRequestBuilder requestBuilder = m_client.prepareGet(url);
        if (m_config.getClientToken() != null) {
            requestBuilder.addHeader(Header.Authorization.toString(), m_config.getClientToken());
        }
        return Observable.from(requestBuilder.execute(builder.build()));
    }
    
    public Observable<String> stream(String url) {
        BoundRequestBuilder requestBuilder = m_client.prepareGet(url);
        if (m_config.getClientToken() != null) {
            requestBuilder.addHeader(Header.Authorization.toString(), m_config.getClientToken());
        }
    	return Observable.create(observer -> {
            requestBuilder.execute(new AsyncHandler<String>() {
			    @Override
			    public STATE onStatusReceived(HttpResponseStatus status) throws Exception {
			        int statusCode = status.getStatusCode();
			        if (statusCode >= 500) {
			            return STATE.ABORT;
			        } else {
			        	return STATE.CONTINUE;
			        }
			    }
			
			    @Override
			    public STATE onHeadersReceived(HttpResponseHeaders h) throws Exception {
			         return STATE.CONTINUE;
			    }
			
			    @Override
			    public STATE onBodyPartReceived(HttpResponseBodyPart bodyPart) throws Exception {
			         observer.onNext(new String(bodyPart.getBodyPartBytes()));
			         return STATE.CONTINUE;
			    }
			
			    @Override
			    public String onCompleted() throws Exception {
			        observer.onCompleted(); 
			    	
			    	return "";
			    }
			
			    @Override
			    public void onThrowable(Throwable t) {
			    	observer.onError(t);
			    }
			});
    	});
    }

    private <T> void invoke(BoundRequestBuilder reqBuilder, Class<T> type, Subscriber<? super T> observer) {
    	if (m_config.getClientToken() != null) {
        	reqBuilder.addHeader(Header.Authorization.toString(), m_config.getClientToken());		
    	}
        Observable.from(reqBuilder.execute())
        	.subscribeOn(Schedulers.io())
            .subscribe(new Subscriber<Response>() {
                @Override
                public void onCompleted() {
                    observer.onCompleted();
                }

                @Override
                public void onError(Throwable t) {
                    observer.onError(t);
                }

                @SuppressWarnings("unchecked")
				@Override
                public void onNext(Response resp) {
                	int statusCode = resp.getStatusCode();
                	if (statusCode == 404 && reqBuilder.build().getMethod().equalsIgnoreCase("get")) {
                        observer.onNext(null);
                        observer.onCompleted();
                	} else if (statusCode < 200 || statusCode >= 300) {
                		// request failed 
                        String body = "<unknown>";
                		try {
                            body = resp.getResponseBody();
                    		// io.fabric8.kubernetes.api.model.base.Status is not exported... 
                            JsonNode json = m_mapper.readTree(body);
                            String message = json.at("/message").textValue();
                            observer.onError(new RuntimeException("Request failed, status: " + statusCode + ", message: " + message + ", from: " + reqBuilder.build().getUrl()));
                        }
                        catch (IOException e) {
                            observer.onError(new RuntimeException("Failed to read object: " + body + " from: " + reqBuilder.build().getUrl(), e));
                        }
                	} else {
                        String body = "<unknown>";
                        try {
                            body = resp.getResponseBody();
                            if(type.equals(String.class)) {
                            	observer.onNext((T) body);
                            } else {
                            	observer.onNext(m_mapper.readValue(body, type));
                            }
                            
                            observer.onCompleted();
                        }
                        catch (IOException e) {
                            observer.onError(new RuntimeException("Failed to read object: " + body + " from: " + reqBuilder.build().getUrl(), e));
                        }
                	}
                }
            });
    }
}
