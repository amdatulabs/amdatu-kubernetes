/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.rest;

import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.AsyncHttpClientConfig.Builder;
import com.ning.http.client.Realm;
import com.ning.http.client.Realm.AuthScheme;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.net.ssl.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Provides the configuration for the {@link KubernetesRestClient}.
 */
class Config {
    private static final String KEY_IN_CLUSTER_CONFIG = "inClusterConfig";
    private static final String KEY_URL = "kubernetesurl";
    private static final String KEY_API_VERSION = "apiversion";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_CLIENT_CA = "clientCA.file";
    private static final String KEY_CLIENT_CA_TYPE = "clientCA.type";
    private static final String KEY_CLIENT_CA_PASSWORD = "clientCA.password";
    private static final String KEY_CLIENT_CERT = "clientCert.file";
    private static final String KEY_CLIENT_CERT_TYPE = "clientCert.type";
    private static final String KEY_CLIENT_CERT_PASSWORD = "clientCert.password";
    private static final String KEY_CLIENT_CERT_KEY_PASSWORD = "clientCert.keyPassword";
    private static final String KEY_ACCEPT_ANY_CERT = "acceptAnyCert";
    private static final String KEY_CLIENT_TOKEN = "clientToken";
    private static final String KEY_NAMESPACE = "namespace";

    private static final String SERVICEACCOUNT_ROOT = "/var/run/secrets/kubernetes.io/serviceaccount";
    private static final String SERVICEACCOUNT_CA_PATH = SERVICEACCOUNT_ROOT + "/ca.crt";
    private static final String SERVICEACCOUNT_TOKEN_PATH = SERVICEACCOUNT_ROOT + "/token";
    private static final String SERVICEACCOUNT_NAMESPACE_PATH = SERVICEACCOUNT_ROOT + "/namespace";
    private static final String ENV_SERVICE_HOST = "KUBERNETES_SERVICE_HOST";
    private static final String ENV_SERVICE_PORT = "KUBERNETES_SERVICE_PORT";

    private static final String DEFAULT_API_VERSION = "v1";

    private URI m_kubernetesUrl;
    private String m_apiVersion;
    private Map<String, String> m_properties;
    
    private static Logger m_logger = LoggerFactory.getLogger(Config.class);

    public Config(Dictionary<String, ?> properties) throws ConfigurationException {

        Boolean inClusterConfig = Boolean.valueOf((String)properties.get(KEY_IN_CLUSTER_CONFIG));
        if(inClusterConfig) {
            m_logger.info("Trying in-cluster config");
            tryInClusterConfig();
            return;
        }

    	m_logger.debug("trying to apply given config");

        URI kubernetesUrl;
        String apiVersion;

        String kubernetesUrlString = getStringValue(properties, KEY_URL, Optional.empty());
        try {
            kubernetesUrl = new URI(kubernetesUrlString);
        }
        catch (URISyntaxException e) {
            throw new ConfigurationException(KEY_URL, "invalid value: not an URI!", e);
        }
        apiVersion = getStringValue(properties, KEY_API_VERSION, Optional.of(DEFAULT_API_VERSION));

        Map<String, String> props = Collections.list(properties.keys()).stream()
                .filter(k -> !KEY_URL.equals(k) && !KEY_API_VERSION.equals(k))
                .collect(Collectors.toMap(Function.identity(), key -> toString(properties.get(key))));

        m_kubernetesUrl = kubernetesUrl;
        m_apiVersion = apiVersion;
        m_properties = props;
        
        trySettingInClusterNamespace();
    }

    private String getStringValue(Dictionary<String, ?> properties, String key, Optional<String> defaultValue) throws ConfigurationException {
        Object value = properties.get(key);
        if(value == null || !(value instanceof String)) {
            if(defaultValue.isPresent()) {
                m_logger.warn("Missing or invalid value for key " + key + " going for default value.");
                return defaultValue.get();
            } else {
                throw new ConfigurationException(key, " missing or invalid value!");
            }
        }
        return (String) value;
    }

    private void tryInClusterConfig() throws ConfigurationException {

    	// try to use in-cluster configuratiom, using ca cert and token mapped into the pod by k8s
    	
        String host = System.getenv(ENV_SERVICE_HOST);
        String port = System.getenv(ENV_SERVICE_PORT);

        File caCert = new File(SERVICEACCOUNT_CA_PATH);
        File token = new File(SERVICEACCOUNT_TOKEN_PATH);
        
        if (host == null || port == null || !caCert.exists() || !token.exists()) {
        	m_logger.info("not running in cluster!");
        	throw new ConfigurationException("na", "config is null, and not running in cluster");        	
        }

        try {
    		String url = "https://" + host + ":" + port;
            URI kubernetesUrl = new URI(url);

            String apiVersion = DEFAULT_API_VERSION;
            Map<String, String> props = new HashMap<>();
            
            BufferedReader tokenReader = new BufferedReader(new FileReader(token));
            StringBuilder bearer = new StringBuilder("Bearer ");
            for (String line = tokenReader.readLine(); line != null; line = tokenReader.readLine()) {
            	bearer.append(line);
            }
            tokenReader.close();
            props.put(KEY_CLIENT_TOKEN, bearer.toString());
            props.put(KEY_CLIENT_CA, caCert.getAbsolutePath());
                	
            m_kubernetesUrl = kubernetesUrl;
            m_apiVersion = apiVersion;
            m_properties = props;

            trySettingInClusterNamespace();
        } catch (Exception e) {
        	m_logger.error("in-cluster config failed!", e);
        	throw new ConfigurationException("na", "error during incluster config: " + e.getMessage(), e);        	        	
        }
        
    	m_logger.debug("in-cluster config done");

	}

    private void trySettingInClusterNamespace() {
        File namespace = new File(SERVICEACCOUNT_NAMESPACE_PATH);
    	if (!namespace.exists()) {
    		return;
    	}
    	
    	try {
	        BufferedReader namespaceReader = new BufferedReader(new FileReader(namespace));
	        StringBuilder ns = new StringBuilder();
	        for (String line = namespaceReader.readLine(); line != null; line = namespaceReader.readLine()) {
	        	ns.append(line);
	        }
	        namespaceReader.close();
	        m_properties.put(KEY_NAMESPACE, ns.toString());
    	} catch (Exception e) {};
    }
    
	/**
     * Creates a new {@link Config} instance.
     */
    public Config(String kubernetesUrl, String apiVersion) {
        m_kubernetesUrl = URI.create(kubernetesUrl);
        m_apiVersion = apiVersion;
        m_properties = Collections.emptyMap();
    }

    private static String toString(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString().trim();
    }

    public AsyncHttpClientConfig createHttpClientConfig() {
        Builder builder = new AsyncHttpClientConfig.Builder()
            .setAllowPoolingConnections(true)
            .setPooledConnectionIdleTimeout(60000)
            .setReadTimeout(300_000)
            .setConnectTimeout(10_000)
            .setRequestTimeout(300_000)
            .setWebSocketTimeout(10_000);
        // In case you really don't care about security...
        builder.setAcceptAnyCertificate(Boolean.parseBoolean(m_properties.get(KEY_ACCEPT_ANY_CERT)));

        if (m_properties.containsKey(KEY_CLIENT_CA) || m_properties.containsKey(KEY_CLIENT_CERT)) {
            try {
                builder.setSSLContext(createSSLContext());
            }
            catch (IOException | ConfigurationException e) {
            	m_logger.error("error setting ssl context!", e);
                throw new RuntimeException("Creating HTTP client configuration failed!", e);
            }
        }

        if (m_properties.containsKey(KEY_USERNAME)) {
            builder.setRealm(new Realm.RealmBuilder()
                .setPrincipal(m_properties.get(KEY_USERNAME))
                .setPassword(m_properties.get(KEY_PASSWORD))
                .setScheme(AuthScheme.BASIC)
                .setUsePreemptiveAuth(true)
                .build());
        }
        
        return builder.build();
    }

    /**
     * @return the version of the Kubernetes API to use, cannot be <code>null</code>.
     */
    public String getApiVersion() {
        return m_apiVersion;
    }

    /**
     * @return the URL to the Kubernetes API server, cannot be <code>null</code>.
     */
    public URI getKubernetesURL() {
        return m_kubernetesUrl;
    }
    
    public String getClientToken() {
    	return m_properties.get(KEY_CLIENT_TOKEN);
    }

    public String getInClusterNamespace() {
    	return m_properties.get(KEY_NAMESPACE);
    }

    private KeyManager[] createKeyManagers() throws IOException, ConfigurationException {
        String clientCert = m_properties.get(KEY_CLIENT_CERT);
        if (clientCert == null || "".equals(clientCert)) {
            return null;
        }
        String type = m_properties.get(KEY_CLIENT_CERT_TYPE);
        if (type == null || "".equals(type)) {
            type = KeyStore.getDefaultType();
        }
        String pwd = m_properties.get(KEY_CLIENT_CERT_PASSWORD);
        char[] ksPwd;
        if (pwd == null) {
            ksPwd = new char[0];
        }
        else {
            ksPwd = pwd.toCharArray();
        }
        pwd = m_properties.get(KEY_CLIENT_CERT_KEY_PASSWORD);
        char[] keyPwd;
        if (pwd == null) {
            keyPwd = new char[0];
        }
        else {
            keyPwd = pwd.toCharArray();
        }

        try {
            KeyStore ks = KeyStore.getInstance(type);
            ks.load(null, ksPwd);
            try (InputStream is = Files.newInputStream(new File(clientCert).toPath())) {
                ks.load(is, ksPwd);
            }

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, keyPwd);
            return kmf.getKeyManagers();
        }
        catch (KeyStoreException e) {
            throw new ConfigurationException(KEY_CLIENT_CA_TYPE, "Invalid or unsupported truststore type: " + type, e);
        }
        catch (GeneralSecurityException e) {
            throw new ConfigurationException(KEY_CLIENT_CA, "Invalid or unsupported truststore!" + type, e);
        }
    }

    private TrustManager[] createTrustManagers() throws IOException, ConfigurationException {
        String clientCA = m_properties.get(KEY_CLIENT_CA);
        if (clientCA == null || "".equals(clientCA)) {
            return null;
        }
        String type = m_properties.get(KEY_CLIENT_CA_TYPE);
        if (type == null || "".equals(type)) {
            type = KeyStore.getDefaultType();
        }
        String pwdStr = m_properties.get(KEY_CLIENT_CA_PASSWORD);
        char[] pwd;
        if (pwdStr == null) {
            pwd = new char[0];
        }
        else {
            pwd = pwdStr.toCharArray();
        }

        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            Collection<? extends Certificate> certificates = certificateFactory.generateCertificates(new FileInputStream(clientCA));
            if (certificates.isEmpty()) {
                throw new IllegalArgumentException("expected non-empty set of trusted certificates");
            }
            KeyStore ks = KeyStore.getInstance(type);
            ks.load(null, pwd);
            int index = 0;
            for (Certificate certificate : certificates) {
                String certificateAlias = "ca" + Integer.toString(index++);
                ks.setCertificateEntry(certificateAlias, certificate);
            }
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);
            return tmf.getTrustManagers();
        }
        catch (KeyStoreException e) {
            throw new ConfigurationException(KEY_CLIENT_CA_TYPE, "Invalid or unsupported truststore type: " + type, e);
        }
        catch (GeneralSecurityException e) {
            throw new ConfigurationException(KEY_CLIENT_CA, "Invalid or unsupported truststore!" + type, e);
        }
    }

    private SSLContext createSSLContext() throws IOException, ConfigurationException {
        try {
            TrustManager[] tms = createTrustManagers();
            KeyManager[] kms = createKeyManagers();

            SSLContext sslCtx = SSLContext.getInstance("TLS");
            sslCtx.init(kms, tms, null);
            return sslCtx;
        }
        catch (KeyManagementException e) {
            throw new ConfigurationException(KEY_CLIENT_CERT, "Invalid client certificate!", e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("JVM does not support TLS or strong PRNGs?", e);
        }
    }
}
