/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.rest;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UrlBuilder {
    private final URI m_baseUrl;
    private final String m_apiVersion;
    private final List<String> m_pathComponents;
    private final Map<String, String> m_labels;
    private String m_namespace;
    private final Map<String, String> m_queryParams;

    public UrlBuilder(URI baseUrl, String apiVersion) {
        if (baseUrl == null) {
            throw new IllegalArgumentException("baseUrl missing!");
        }
        if (apiVersion == null) {
            throw new IllegalArgumentException("apiVersion missing!");
        }

        m_baseUrl = baseUrl;
        m_apiVersion = apiVersion;

        m_pathComponents = new ArrayList<>();
        m_labels = new LinkedHashMap<>();
        m_queryParams = new LinkedHashMap<>();
    }

    public String build() {
        return build(m_baseUrl.getScheme());
    }

    public String buildWs() {
        String scheme = "ws";
        if ("https".equals(m_baseUrl.getScheme())) {
            scheme = "wss";
        }
        
        m_queryParams.put("watch", "true");
        return build(scheme);
    }

    public UrlBuilder labels(Map<String, String> labels) {
    	if(labels != null) {
    	
	        m_labels.putAll(labels.entrySet().stream()
	            .collect(Collectors.<Map.Entry<String, String>, String, String> toMap(
	                kv -> encode(kv.getKey()),
	                kv -> encode(kv.getValue()))));
    	}
        return this;
    }

    public UrlBuilder label(String name, String value) {
        m_labels.put(encode(name), encode(value));

        return this;
    }
    
    public UrlBuilder query(String name, String value) {
        m_queryParams.put(encode(name), encode(value));

        return this;
    }

    public UrlBuilder namespace(String ns) {
        int start = 0, end = ns.length();
        if (ns.startsWith("/")) {
            start++;
        }
        if (ns.endsWith("/")) {
            end--;
        }
        m_namespace = ns.substring(start, end);
        return this;
    }

    public UrlBuilder path(String path) {
        m_pathComponents.addAll(normalize(path));

        return this;
    }

    private static String encode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported by this JVM?!");
        }
    }

    private static List<String> normalize(String path) {
        return Arrays.stream(path.split("/"))
            .filter(s -> s != null && !"".equals(s.trim()))
            .collect(Collectors.toList());
    }

    private String build(String scheme) {
        if (scheme == null) {
            throw new IllegalArgumentException("Scheme cannot be null!");
        }

        StringBuilder path = new StringBuilder("api/").append(m_apiVersion);
        if (m_namespace != null) {
            path.append("/namespaces/").append(m_namespace);
        }
        if (!m_pathComponents.isEmpty()) {
            path.append("/").append(String.join("/", m_pathComponents));
        }

        StringBuilder query = new StringBuilder();
        if (!m_labels.isEmpty()) {
            query.append("labelSelector=");

            query.append(String.join(",", m_labels.entrySet().stream()
                .map(kv -> {
                    return kv.getKey() + "%3D" + kv.getValue();
                })
                .toArray(String[]::new)));
        }
        
        m_queryParams.forEach((k,v) -> {
        	if(query.length() > 0) {
    		   query.append("&");	
        	}
        	
    		query.append(k).append("=").append(v);
    	});


        StringBuilder sb = new StringBuilder(m_baseUrl.toASCIIString());
        if (path.length() != 0) {
            if (sb.charAt(sb.length() - 1) != '/') {
                sb.append("/");
            }
            sb.append(path);
        }
        if (query != null && query.length() > 0) {
            sb.append("?").append(query);
        }
        // Use correct scheme...
        int idx = sb.indexOf(":");
        if (idx < 1) {
            throw new IllegalArgumentException("Invalid URI: no colon found?!");
        }
        return sb.replace(0, idx, scheme).toString();
    }

}
