/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kubernetes.rest;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import org.amdatu.kubernetes.Kubernetes;
import org.amdatu.kubernetes.LogOptions;
import org.amdatu.kubernetes.WatchUpdate;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceList;
import io.fabric8.kubernetes.api.model.NodeList;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.ReplicationControllerList;
import io.fabric8.kubernetes.api.model.ResourceQuotaList;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.SecretBuilder;
import io.fabric8.kubernetes.api.model.SecretList;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceList;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

@Component(provides={ManagedService.class}, properties=@Property(name=Constants.SERVICE_PID, value="org.amdatu.kubernetes"))
public class KubernetesRestClient implements Kubernetes, ManagedService {
    private volatile Config m_config;
    // Managed in start/stop
    private volatile HttpClient m_httpClient;
    
    @Inject
    private volatile DependencyManager m_dm;
    
    private static Logger m_logger = LoggerFactory.getLogger(KubernetesRestClient.class);

    public KubernetesRestClient() {
        // Used by Felix DM...
    }

    public KubernetesRestClient(String kubernetesUrl, String apiVersion) {
        m_config = new Config(kubernetesUrl, apiVersion);
        m_httpClient = new HttpClient(m_config);
    }

    private static void sleep(long msecs) {
        try {
            TimeUnit.MILLISECONDS.sleep(msecs);
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public Observable<Namespace> createNamespace(Namespace ns) {
        String url = createUrlBuilder().path("namespaces").build();

        return Observable.create(observer -> m_httpClient.postObject(url, ns, observer));
    }

    @Override
    public Observable<ReplicationController> createReplicationController(String namespace, ReplicationController rc) {
        String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").build();

        return Observable.create(observer -> m_httpClient.postObject(url, rc, observer));
    }

    @Override
    public Observable<Boolean> deleteNamespace(String namespace) {
        String url = createUrlBuilder().namespace(namespace).build();
        return Observable.<Integer> create(observer -> m_httpClient.deleteObject(url, observer))
        		.map(f -> ((f.intValue() >= 200) && (f.intValue() <= 300)));
    }

    @Override
    public Observable<Pod> getPod(String namespace, String name) {
        String url = createUrlBuilder().namespace(namespace).path("pods").path(name).build();

        return Observable.create(observer -> m_httpClient.getObject(url, Pod.class, observer));
    }
    
    @Override
   	public Observable<Boolean> deletePod(String namespace, String name) {
    	return deleteResource(namespace, "pods", name);
    }

    @Override
    public Observable<ReplicationController> getReplicationController(String namespace, String name) {
        String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").path(name).build();

        return Observable.create(observer -> m_httpClient.getObject(url, ReplicationController.class, observer));
    }
    
    @Override
	public Observable<ReplicationControllerList> listReplicationControllers(String namespace) {
    	String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").build();

        return Observable.create(observer -> m_httpClient.getObject(url, ReplicationControllerList.class, observer));
	}
    
    @Override
	public Observable<ReplicationControllerList> listReplicationControllers(String namespace, Map<String, String> labelSelector) {
    	String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").labels(labelSelector).build();

        return Observable.create(observer -> m_httpClient.getObject(url, ReplicationControllerList.class, observer));
	}

    @Override
    public Observable<NamespaceList> listNamespaces() {
        String url = createUrlBuilder().path("namespaces").build();

        return Observable.create(observer -> m_httpClient.getObject(url, NamespaceList.class, observer));
    }

    @Override
    public Observable<NodeList> listNodes() {
        String url = createUrlBuilder().path("nodes").build();

        return Observable.create(observer -> m_httpClient.getObject(url, NodeList.class, observer));
    }

    @Override
    public Observable<PodList> listPods(String namespace, Map<String, String> labelSelector) {
        String url = createUrlBuilder().namespace(namespace).path("pods").labels(labelSelector).build();

        return Observable.create(observer -> m_httpClient.getObject(url, PodList.class, observer));
    }
    
    @Override
    public Observable<ReplicationController> scale(String namespace, String rcName, int nrOfReplicas) {
        return Observable.create(subscriber -> {
            getReplicationController(namespace, rcName).subscribe(rc -> {
                rc.getSpec().setReplicas(nrOfReplicas);

                updateReplicationController(namespace, rc).subscribe(updated -> {
                    subscriber.onNext(updated);
                    subscriber.onCompleted();
                } ,
                    t -> subscriber.onError(t));
            } ,
                t -> subscriber.onError(t));
        });
    }
    
    /**
     * Managed by Felix DM.
     */
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        if(properties == null) {
            return;
        }
        m_config = new Config(properties);

        try {
            m_logger.debug("init http client");
            m_httpClient = new HttpClient(m_config);        	
        } catch (Throwable e) {
        	m_logger.error("error initializing http client", e);
        	throw new ConfigurationException("na", "init http client failed", e);
        }

        try {
        	m_logger.debug("registering kubernetes component");
            this.m_dm.add(
            		m_dm.createComponent()
            			.setInterface(Kubernetes.class.getName(), null)
            			.setImplementation(this)
            		);        	
        } catch (Throwable e) {
        	m_logger.error("error registering kubernetes component", e);
        	throw new ConfigurationException("na", "registering kubernetes component failed", e);
        }
        
    }

    @Override
    public Observable<ReplicationController> updateReplicationController(String namespace, ReplicationController rc) {
        String url = createUrlBuilder().namespace(namespace).path("replicationcontrollers").path(rc.getMetadata().getName()).build();

        rc.getMetadata().setResourceVersion(null);
        rc.setStatus(null);

        return Observable.create(observer -> m_httpClient.putObject(url, rc, observer));
    }

    @Override
    public Observable<PodList> waitForPods(String namespace, Map<String, String> labelSelector, int expected, Predicate<Pod> filter) {
        AtomicInteger backupTime = new AtomicInteger(100);

        return Observable.<PodList> create(subscriber -> {
            listPods(namespace, labelSelector).subscribe(new Subscriber<PodList>() {
                @Override
                public void onCompleted() {
                    subscriber.onCompleted();
                }

                @Override
                public void onError(Throwable t) {
                    subscriber.onError(t);
                }

                @Override
                public void onNext(PodList list) {
                    if (list.getItems().stream().filter(filter).count() == expected) {
                        subscriber.onNext(list);
                        subscriber.onCompleted();
                    }
                    else {
                        sleep(backupTime.getAndUpdate(v -> v * 2));
                        subscriber.onError(new RuntimeException("Not enough pods found"));
                    }
                }
            });
        }).retry(10);
    }

    @Override
    public Observable<WatchUpdate<Pod>> watchPods(String namespace, Map<String, String> labelSelector) {
        String url = createUrlBuilder().namespace(namespace).path("pods").buildWs();

        return Observable.<WatchUpdate<Pod>> create(subscriber -> m_httpClient.watchObject(url, Pod.class, subscriber));
    }

	@Override
	public Observable<SecretList> listSecrets(String namespace) {
        String url = createUrlBuilder().namespace(namespace).path("secrets").build();

        return Observable.create(observer -> m_httpClient.getObject(url, SecretList.class, observer));
	}

	@Override
	public Observable<Secret> getSecret(String namespace, String name) {
        String url = createUrlBuilder().namespace(namespace).path("secrets").path(name).build();

        return Observable.create(observer -> m_httpClient.getObject(url, Secret.class, observer));
	}

	@Override
	public Observable<Secret> setSecret(String namespace, String name, Map<String, String> data) {
		String url;
		Secret secret = getSecret(namespace, name).toBlocking().single();
		if (secret == null) {
			// create new Secret
			ObjectMeta metadata = new ObjectMeta();
			metadata.setName(name);
			
			Secret newSecret = new SecretBuilder().withData(data).withType("Opaque").withMetadata(metadata).withKind("Secret").build();
			url = createUrlBuilder().namespace(namespace).path("secrets").build();
	        return Observable.create(observer -> m_httpClient.postObject(url, newSecret, observer));
		} else {
			// update existing Secret
			secret.setData(data);
			url = createUrlBuilder().namespace(namespace).path("secrets").path(name).build();
	        return Observable.create(observer -> m_httpClient.putObject(url, secret, observer));
		}
	}
	
	@Override
	public Observable<Secret> setSecret(String namespace, Secret secret) {
		String name = secret.getMetadata().getName();
		return getSecret(namespace, name).
				flatMap(oldSecret -> setSecret(namespace, secret, oldSecret));

	}

	private Observable<Secret> setSecret(String namespace, Secret newSecret, Secret oldSecret) {
		String name = newSecret.getMetadata().getName();
		if (oldSecret == null) {
			// new Secret
			String url = createUrlBuilder().namespace(namespace).path("secrets").build();
	        return Observable.create(observer -> m_httpClient.postObject(url, newSecret, observer));
		} else {
			// update existing Secret
			oldSecret.setData(newSecret.getData());
			String url = createUrlBuilder().namespace(namespace).path("secrets").path(name).build();
	        return Observable.create(observer -> m_httpClient.putObject(url, oldSecret, observer));
		}
	}
	
	@Override
	public Observable<Boolean> deleteSecret(String namespace, String name) {
		return deleteResource(namespace, "secrets", name);
	}

	@Override
	public Observable<String> getLogs(String namespace, String name) {
		String url = createUrlBuilder().namespace(namespace).path("pods").path(name).path("log").build();
        return Observable.create(observer -> m_httpClient.getString(url, observer));
	}
	
	@Override
	public Observable<String> getLogs(String namespace, String name, String containerName) {
		String url = createUrlBuilder().namespace(namespace).path("pods").path(name).path("log").query("container", containerName).build();
        return Observable.create(observer -> m_httpClient.getString(url, observer));
	}
	
	@Override
	public Observable<String> getLogs(String namespace, String name, String containerName, LogOptions options) {
		UrlBuilder query = createUrlBuilder().namespace(namespace).path("pods").path(name).path("log").query("container", containerName);
		
		
		if(options.isPrevious()) {
			query.query("previous", "true");
		}
			
		if(options.isTimestamps()) {
			query.query("timestmaps", "true");
		}
		
		if(options.getLimitBytes() != 0) {
			query.query("limitBytes", "" + options.getLimitBytes());
		}
		
		if(options.getSince() != 0) {
			query.query("sinceSeconds", "" + options.getSince());
		}
		
		if(options.getSinceTime() != null) {
			query.query("sinceTime", options.getSinceTime());
		}
		
		if(options.getTail() != 0) {
			query.query("tailLines", "" + options.getTail());
		}
		
		if(options.isFollow()) {
			query.query("follow", "true");
		}
		
		String url = query.build();
		return m_httpClient.stream(url);
	}
	
	/** Services **/
	
	@Override
	public Observable<Service> createService(String namespace, Service service) {
		String url = createUrlBuilder().namespace(namespace).path("services").build();	
		return Observable.create(observer -> m_httpClient.postObject(url, service, observer));
	}

	@Override
	public Observable<Service> getService(String namespace, String name) {
		String url = createUrlBuilder().namespace(namespace).path("services").path(name).build();
		return Observable.create(observer -> m_httpClient.getObject(url, Service.class, observer));
	}

	@Override
	public Observable<Service> updateService(String namespace, Service service) {
		String url = createUrlBuilder().namespace(namespace).path("services").path(service.getMetadata().getName()).build();
		
		service.setStatus(null);
		
		// use PATCH instead of PUT to update a k8s service
		// PUT will replace the entire object
		// when creating a Service object, some of the fields are not accessible (e.g. spec.clusterIP) and will 
		// get a default value which triggers an error on the server 
		return Observable.create(observer -> m_httpClient.patchObject(url, service, observer));
	}

	@Override
	public Observable<Boolean> deleteService(String namespace, String name) {
		return deleteResource(namespace, "services", name);
	}

	@Override
	public Observable<ServiceList> listServices(String namespace) {
		String url = createUrlBuilder().namespace(namespace).path("services").build();
        return Observable.create(observer -> m_httpClient.getObject(url, ServiceList.class, observer));
	}


	@Override
	public Observable<ServiceList> listServices(String namespace, Map<String, String> labelSelector) {
		String url = createUrlBuilder().namespace(namespace).path("services").labels(labelSelector).build();
		return Observable.create(observer -> m_httpClient.getObject(url, ServiceList.class, observer));
	}
	
    private Observable<Boolean> deleteResource(String namespace, String resourceType, String name) {
        String url = createUrlBuilder().namespace(namespace).path(resourceType).path(name).build();

        return Observable.<Integer> create(observer -> m_httpClient.deleteObject(url, observer)).doOnNext(n -> {
        	System.out.println(url);
         	System.out.println("Next:");
        	System.out.println(n);
        })
        .map(f -> ((f.intValue() >= 200) && (f.intValue() <= 300)));
    }

    private UrlBuilder createUrlBuilder() {
        Config cfg = m_config;
        return new UrlBuilder(cfg.getKubernetesURL(), cfg.getApiVersion());
    }

	@Override
	public Observable<ResourceQuotaList> listQuota(String namespace) {
		String url = createUrlBuilder().namespace(namespace).path("resourcequotas").build();
		return Observable.create(observer -> m_httpClient.getObject(url, ResourceQuotaList.class, observer));
	}

	@Override
	public String getInClusterNamespace() {
		return m_config.getInClusterNamespace();
	}

    @Override
    public String getKubernetesMasterUrl() {
        return m_config.getKubernetesURL().toString();
    }

    @Override
    public Observable<Boolean> isHealthy() {
        return Observable.<Integer> create(observer -> m_httpClient.getStatus(m_config.getKubernetesURL() + "/healthz", observer))
                .timeout(5, TimeUnit.SECONDS, Schedulers.io())
                .map(code -> {
                    m_logger.info("Kubernetes healthcheck returned " + code);
                    return ( (code.intValue() >= 200) && (code.intValue() <=300));
                });
    }

    /**
     * Managed by Felix DM and called when this component is started.
     */
    @Start
    protected final void start() {
        // Nop
    }

    /**
     * Managed by Felix DM and called when this component is stopped.
     */
    @Stop
    protected final void stop() {
        if (m_httpClient != null) {
            m_httpClient.close();
            m_httpClient = null;
        }
    }

}
