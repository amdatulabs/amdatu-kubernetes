package org.amdatu.kubernetes.examples;

import org.amdatu.kubernetes.Kubernetes;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

@Component
public class ListPods {

	@ServiceDependency
	private volatile Kubernetes kubernetes;
	
	@Start
	void listPods() {
		System.out.println("Listing pods!");
		try {
			kubernetes.listPods(kubernetes.getInClusterNamespace(), null).subscribe(
					next -> next.getItems()
						.forEach(pod -> System.out.println(pod.getMetadata().getName())),
					err -> System.err.println(err)
			);			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
}
